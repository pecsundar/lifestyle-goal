# LifeStyle Demo

> cordova-vuejs project

## Build Setup

Step to run the Project in IOS simulator

``` bash
# clone the project into your local
git@gitlab.com:pecsundar/lifestyle-goal.git

#to build the project, go into the myApp folder to yarn install and build
cd lifestyle-goal/myApp

yarn install
yarn run build

#To view the outcome, go back to the root lifestyle-goal folder

cd ..

# This will run the project in browser
cordova platform add browser #cordova add ios #cordova add android
cordova run browser #cordova build ios #cordova build android

# This will run the project in ios 
cordova platform add ios
cordova build ios
cordova run ios
#Once the build is ready, you can either choose to use the Xcode to run the simulation or use cordova emulate ios to see the app. More details refer here https://cordova.apache.org/docs/en/5.1.1/guide/platforms/ios/index.html

# This will run the project in Android 
cordova platform add android
cordova build android
cordova run android

```
## About the project
Tech used in this projects
*Cordova
*Vue js with UI package Onsen UI
*Jest for unit test


For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
