import Vue from 'vue';

import Mint from 'mint-ui';
import 'mint-ui/lib/style.css';

import VueOnsen from 'vue-onsenui';
import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';


import App from './App';
import router from './router';

Vue.config.productionTip = false;
Vue.use(Mint);
Vue.use(VueOnsen);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
